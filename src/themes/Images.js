const images = {
  icNotif: require('../images/ic_notif.png'),
  icSearch: require('../images/ic_search.png'),
  icMember: require('../images/ic_member.png'),
  icCalendar: require('../images/ic_calendar.png'),
  icTime: require('../images/ic_time.png')
}

export default images