const fonts = {
  poppinsBlack: 'Poppins-Black',
  poppinsBlackItalic: 'Poppins-BlackItalic',
  poppinsBold: 'Poppins-Bold',
  poppinsBoldItalic: 'Poppins-BoldItalic',
  poppinsExtraBold: 'Poppins-ExtraBold',
  poppinsExtraBoldItalic: 'Poppins-ExtraBoldItalic',
  poppinsExtraLight: 'Poppins-ExtraLight',
  poopinsExtraLightItalic : 'Poppins-ExtraLightItalic',
  poopinsItalic: 'Poppins-Italic',
  poopinsLight : 'Poppins-Light',
  poopinsLightItalic:'Poppins-LightItalic',
  poopinsMedium: 'Poppins-Medium',
  poopinsMediumItalic: 'Poppins-MediumItalic',
  poopinsRegular: 'Poppins-Regular',
  poopinSemiBold: 'Poppins-SemiBold',
  poopinsSemiBoldItalic :'Poppins-SemiBoldItalic',
  poopinsThin: 'Poppins-Thin',
  poopinsThinItalic : 'Poppins-ThinItalic'
}

export default fonts