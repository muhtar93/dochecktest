const colors = {
  primaryText: '#D2D7DB',
  primary: '#223151',
  white: '#FFFFFF',
  grey: '#606271',
  grey2: '#6B7488',
  grey3: '#1F2221',
  line: 'rgba(204, 210, 227, 0.27)',
  purple: '#E3EBFF',
  red: '#FF968E',
  yellow: '#F9C917',
  grey4: '#F7FBFE',
  black: '#1F2221'
}

export default colors