import React from 'react'
import {Text, View, StyleSheet} from 'react-native'
import { Colors, Fonts } from '../themes'

const TitleHome = ({title, subTitle}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.textTitle}>{title}</Text>
      <Text style={styles.textSeeAll}>{subTitle}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  textTitle: {
    fontFamily: Fonts.poppinsBold,
    color: Colors.black,
    fontSize: 16
  },
  textSeeAll: {
    fontFamily: Fonts.poppinsBold,
    color: Colors.primaryText,
    fontSize: 12
  }
})

export default TitleHome