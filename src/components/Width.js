import React from 'react'
import {View} from 'react-native'

const Width = ({size}) => {
  return (
    <View style={{width: size}}/>
  )
}

export default Width