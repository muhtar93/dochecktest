import React from 'react'
import {Text, View, Image, StyleSheet} from 'react-native'
import { Colors, Fonts, Images } from '../themes'

const Header = () => {
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.textUser}>Hi, Apriliani</Text>
        <Text style={styles.textJob}>UI Designer</Text>
      </View>
      <View>
        <Image 
          source={Images.icNotif}
          style={styles.image}
        />
        <View style={styles.badges}/>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  image: {
    width: 24,
    height: 24
  },
  textUser: {
    fontFamily: Fonts.poppinsBold,
    fontSize: 16,
    color: Colors.grey3
  },
  textJob: {
    fontFamily: Fonts.poopinsRegular,
    fontSize: 14,
    color: Colors.primaryText
  },
  badges: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: Colors.yellow,
    top: -20,
    left: 16
  }
})

export default Header