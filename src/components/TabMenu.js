import React from 'react'
import {Text, TouchableOpacity, View, StyleSheet} from 'react-native'
import { Colors, Fonts } from '../themes'

const TabMenu = ({onPress, title, color}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={styles.text}>{title}</Text>
      <View
        style={{
          width: 4,
          height: 4,
          borderRadius: 2,
          backgroundColor: color,
          alignSelf: 'center',
          marginTop: 4
        }}
      />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  text: {
    fontFamily: Fonts.poopinsRegular,
    color: Colors.primary
  }
})

export default TabMenu