import React from 'react'
import {Text, View, StyleSheet, Image} from 'react-native'
import { Colors, Images } from '../themes'
import * as Progress from 'react-native-progress'

const Item = ({ title }) => (
  <View style={styles.container}>
    <Text>{title}</Text>
    <View style={styles.containerMember}>
      <Image 
        source={Images.icMember}
        resizeMode='contain'
        style={styles.image}
      />
      <View>
        <View style={styles.containerCal}>
          <Image 
            source={Images.icCalendar}
            resizeMode='contain'
            style={styles.imgCal}
          />
          <Text>Aug 23, 2021</Text>
        </View>
        <View style={styles.containerCal}>
          <Image 
            source={Images.icTime}
            resizeMode='contain'
            style={styles.imgCal}
          />
          <Text>13.30</Text>
        </View>
      </View>
      <Progress.Bar progress={0.3} width={200} />
    </View>
  </View>
)

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.primary, 
    marginRight: 16, 
    padding: 16, 
    borderRadius: 12, 
    width: 150,
    height: 150
  },
  image: {
    height: 32,
    width: 64
  },
  containerMember: {
    flexDirection: 'row'
  },
  imgCal: {
    width: 12,
    height: 12,
    tintColor: Colors.white
  },
  containerCal: {
    flexDirection: 'row'
  }
})

export default Item