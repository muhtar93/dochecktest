import React from 'react'
import {Text, View, StyleSheet, Image} from 'react-native'

const StartedImg = () => {
  return (
    <Image 
      style={{height: 200, width: 300}}
      source={require('../images/ic_welcome.png')}
    /> 
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#223151',
    margin: 24,
    height: 60,
    borderRadius: 24,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 14,
    color: 'white',
    marginRight: 24,
    fontFamily: 'Poppins-Bold'
  }
})

export default StartedImg
