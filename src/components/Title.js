import React from 'react'
import {Text, View, StyleSheet} from 'react-native'
import {Colors, Fonts} from '../themes'

const Title = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Smart task {'\n'}management</Text>
      <Text style={styles.textSub}>This smart tool is designed to help you {'\n'}better manage your tasks</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingLeft: 24,
    marginTop: 32
  },
  text: {
    fontSize: 32,
    color: Colors.primary,
    fontFamily: Fonts.poppinsBold,
    marginBottom: 8
  },
  textSub: {
    fontSize: 14,
    color: Colors.primaryText,
    fontFamily: Fonts.poopinsRegular
  }
})

export default Title
