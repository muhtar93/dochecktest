import React from 'react'
import {Text, StyleSheet, TouchableOpacity, View, Image} from 'react-native'
import {Colors, Fonts} from '../themes'

const Menu = ({title, source, color}) => {
  return (
    <TouchableOpacity style={{flexDirection: 'row', justifyContent: 'space-between'}}>
      <View style={{flexDirection: 'row'}}>
        <View style={{backgroundColor: color, padding: 8, borderRadius: 8}}>
          <Image 
            source={source}
            style={{width: 30, height: 30}}
            resizeMode='contain'
          />
        </View>
        <Text style={{alignSelf: 'center', marginLeft: 8, fontFamily: Fonts.poopinSemiBold}}>{title}</Text>
      </View>
      <Image 
            source={require('../images/ic_arrow.png')}
            style={{width: 16, height: 16, alignSelf: 'center'}}
            resizeMode='contain'
          />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  text: {
    alignSelf: 'flex-end',
    fontSize: 14,
    marginRight: 24,
    fontFamily: Fonts.poopinsRegular
  }
})

export default Menu
