import React from 'react'
import {Text, View} from 'react-native'

const Height = ({size}) => {
  return (
    <View style={{height: size}}/>
  )
}

export default Height