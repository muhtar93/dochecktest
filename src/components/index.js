import Skip from './Skip'
import StartedBtn from './StartedBtn'
import StartedImg from './StartedImg'
import Title from './Title'
import Total from './Total'
import Menu from './Menu'
import Header from './Header'
import Search from './Search'
import TitleHome from './TitleHome'
import TabMenu from './TabMenu'
import Width from './Width'
import Item from './Item'

export {
  Skip, Title, StartedBtn, StartedImg, Total, Menu, Header, Search, TitleHome,
  TabMenu, Width, Item
}