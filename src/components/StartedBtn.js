import React from 'react'
import {Text, StyleSheet, TouchableOpacity} from 'react-native'
import {Colors, Fonts} from '../themes'

const StartedBtn = ({onPress}) => {
  return (
    <TouchableOpacity 
      style={styles.container}
      onPress={onPress}
    >
      <Text style={styles.text}>Get Started</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.primary,
    margin: 24,
    height: 60,
    borderRadius: 24,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 14,
    color: Colors.white,
    marginRight: 24,
    fontFamily: Fonts.poppinsBold
  }
})

export default StartedBtn
