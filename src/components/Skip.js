import React from 'react'
import {Text, StyleSheet, TouchableOpacity} from 'react-native'
import {Colors, Fonts} from '../themes'

const Skip = ({onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={styles.text}>Skip {'>'}{'>'}</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  text: {
    alignSelf: 'flex-end',
    fontSize: 14,
    color: Colors.primaryText,
    marginRight: 24,
    fontFamily: Fonts.poopinsRegular
  }
})

export default Skip
