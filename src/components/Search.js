import React from 'react'
import {TextInput, View, StyleSheet, Image} from 'react-native'
import { Colors, Images } from '../themes'

const Search = () => {
  return (
    <View style={styles.container}>
      <TextInput 
        placeholder='Search here..'
      />
      <Image 
        source={Images.icSearch}
        style={styles.image}
        resizeMode='contain'
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.grey4,
    height: 42,
    borderRadius: 16,
    paddingHorizontal: 24,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  image: {
    width: 16,
    height: 16,
    alignSelf: 'center'
  }
})
export default Search