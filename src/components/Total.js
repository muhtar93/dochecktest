import React from 'react'
import {Text, StyleSheet, TouchableOpacity, View, Image} from 'react-native'
import {Colors, Fonts} from '../themes'

const Total = ({title, project, value, source}) => {
  return (
    <View>
      <Text style={styles.text}>{title}</Text>
      <View style={styles.container}>
        <Image 
          source={source}
          style={{width: 32, height: 32, alignSelf: 'center'}}
        />
        <View style={{marginLeft: 8}}>
          <Text style={styles.textMyProject}>{project}</Text>
          <Text style={styles.textNumber}>{value}</Text>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row', 
    marginTop: 8,     
    borderRadius: 12, 
    backgroundColor: Colors.white,
    padding: 12
  },
  text: {
    fontSize: 14,
    color: Colors.gray,
    fontFamily: Fonts.poopinsMedium
  },
  textMyProject: {
    fontFamily: Fonts.poopinSemiBold,
    color: Colors.gray2,
    fontSize: 12
  },
  textNumber: {
    fontFamily: Fonts.poppinsBold,
    color: Colors.primary,
    fontSize: 16
  }
})

export default Total
