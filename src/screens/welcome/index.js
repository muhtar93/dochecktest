import React from 'react'
import {View, StyleSheet, SafeAreaView} from 'react-native'
import {Skip, Title, StartedBtn, StartedImg} from '../../components'
import { Colors } from '../../themes'

const WelcomeScreen = ({navigation}) => {

  const next = () => {
    navigation.navigate('BottomTab')
  }

  return (
    <SafeAreaView style={styles.container}>
    <View style={styles.container}>
      <View style={styles.containerTop}>
        <Skip 
          onPress={next}
        />
        <Title />
      </View>
      <View style={styles.containerCenter}>
        <StartedImg />
      </View>
      <View style={styles.containerBottom}>
        <StartedBtn 
          onPress={next}
        />
      </View>
    </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white
  },
  containerTop: {
    flex: 0.3
  },
  containerCenter: {
    flex: 0.6,
    justifyContent: 'center',
    alignItems: 'center'
  },
  containerBottom: {
    flex: 0.2,
    justifyContent: 'center'
  }
})

export default WelcomeScreen