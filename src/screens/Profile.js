import React from 'react'
import {Text, SafeAreaView, StyleSheet, View, Image, TouchableOpacity} from 'react-native'
import { Gap, Menu, Total } from '../components'
import {Colors, Fonts} from '../themes'

const Profile = () => {
  return (
    <SafeAreaView>
      <View style={{paddingHorizontal: 24}}>
      <Text style={styles.myProfileText}>My Profile</Text>
      <View style={styles.containerPhoto}>
        <Image
          source={require('../images/ic_photo.png')}
          resizeMode={'contain'}
          style={{width: 100, height: 100}}
        />
      </View>
      <TouchableOpacity>
        <Image 
          source={require('../images/ic_edit.png')}
          resizeMode={'contain'}
          style={{width: 32, height: 32, alignSelf: 'center', top: -30, marginLeft: 80}}
        />
      </TouchableOpacity>
      <Text style={styles.userText}>My Profile</Text>
      <Text style={styles.jobText}>UI Designer</Text>
      <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
        <Total
          title= 'Total Project'
          project= 'My Project'
          value= '+42'
          source={require('../images/ic_task.png')}
        />
        <View style={{width: 64}}/>
        <Total 
          title= 'Total Team'
          project= 'My Team'
          value= '+10'
          source={require('../images/ic_team.png')}
        />
      </View>
      <View 
        style={{height: 2, backgroundColor: Colors.line, marginVertical: 32}}
      />
      <Menu 
        title='Settings'
        source={require('../images/ic_setting.png')}
        color={Colors.purple}
      />
      <View style={{height: 16}}/>
      <Menu 
        title= 'User Management'
        source={require('../images/ic_um.png')}
        color={Colors.purple}
      />
      <View style={{height: 16}}/>
      <Menu 
        title= 'Privacy'
        source={require('../images/ic_privacy.png')}
        color={Colors.purple}
      />
      <View style={{height: 16}}/>
      <Menu 
        title= 'Log Out'
        source={require('../images/ic_logout.png')}
        color={Colors.red}
      />
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  myProfileText: {
    fontFamily: Fonts.poppinsBold,
    alignSelf: 'center',
    fontSize: 18
  },
  containerPhoto: {
    width: 120,
    height: 120,
    borderRadius: 60,
    alignSelf: 'center',
    backgroundColor: Colors.primaryText,
    marginTop: 16,
    justifyContent: 'center',
    alignItems: 'center'
  },
  userText: {
    fontFamily: Fonts.poopinsMedium,
    fontSize: 18,
    marginTop: 16,
    alignSelf: 'center',
    top: -24
  },
  jobText: {
    fontFamily: Fonts.poopinsMedium,
    fontSize: 16,
    marginTop: 16,
    alignSelf: 'center',
    top: -32,
    color: '#6B7488'
  }
})

export default Profile