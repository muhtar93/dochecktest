import React from 'react'
import {Text, View, SafeAreaView, StyleSheet, FlatList} from 'react-native'
import { Images } from '../../themes'

const DATA = [
  {
    id: 1,
    title: 'UI Dashboard Projects',
    members: 'Images.icNotif',
    date: 'Aug 23, 2021',
    time: 13.30,
    status: 'In progress',
    progress: '88%'
  },
  {
    id: 2,
    title: 'Landing page',
    members: Images.icNotif,
    date: 'Aug 23, 2021',
    time: 12.30,
    status: 'Coming soon',
    progress: '48%'
  }
]

const Item = ({ title }) => (
  <View >
    <Text>{title}</Text>
  </View>
)

const Upcoming = () => {
  const renderItem = ({ item }) => (
    <Item title={item.title} />
  )
  
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

export default Upcoming