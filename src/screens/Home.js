import React, {useState} from 'react'
import {SafeAreaView, View, StyleSheet, FlatList} from 'react-native'
import {Header, Search, TabMenu, TitleHome, Width} from '../components'
import Height from '../components/Height'
import { Colors } from '../themes'
import Goal from './Goal'
import Done from './goals/Done'
import InProgress from './goals/InProgress'
import Todo from './goals/Todo'
import Upcoming from './goals/Upcoming'
import Profile from './Profile'

const Home = () => {
  const[value, setValue] = useState('InProgress')
  const[colorInProgress, setColorInProgress] = useState(Colors.yellow)
  const[colorTodo, setColorTodo] = useState(null)
  const[colorDone, setColorDone] = useState(null)
  const[colorUpcoming, setColorUpcoming] = useState(null)

  const renderTab = () => {
    if(value === 'InProgress') {
      return (
        <InProgress />
      )
    } else if(value === 'Todo') {
      return (
        <Todo />
      )
    } else if(value === 'Done') {
      return (
        <Done />
      )
    }
    else {
      return (
        <Upcoming />
      )
    }
  }

  const onPressInProgress = () => {
    setValue('InProgress')
    setColorInProgress(Colors.yellow)
    setColorTodo(null)
    setColorDone(null)
    setColorUpcoming(null)
  }

  const onPressTodo = () => {
    setValue('Todo')
    setColorInProgress(null)
    setColorTodo(Colors.yellow)
    setColorDone(null)
    setColorUpcoming(null)
  }

  const onPressDone = () => {
    setValue('Done')
    setColorInProgress(null)
    setColorTodo(null)
    setColorDone(Colors.yellow)
    setColorUpcoming(null)
  }

  const onPressUpcoming = () => {
    setValue('Upcoming')
    setColorInProgress(null)
    setColorTodo(null)
    setColorDone(null)
    setColorUpcoming(Colors.yellow)
  }

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <Header />
        <Height size={24} />
        <Search />
        <Height size={24} />
        <TitleHome 
          title='My Goals'
          subTitle='See all'
        />
        <Height size={16} />
        <View style={styles.containerTab}>
          <TabMenu 
            onPress={onPressInProgress}
            title='In Progress'
            color={colorInProgress}
          />
          <Width size={32} />
          <TabMenu 
            onPress={onPressTodo}
            title='To do'
            color={colorTodo}
          />
          <Width size={32} />
          <TabMenu
            onPress={onPressDone}
            title='Done'
            color={colorDone}
          />
          <Width size={32} />
          <TabMenu 
            onPress={onPressUpcoming}
            title='Upcoming'
            color={colorUpcoming}
          />
        </View>
        <View style={{height: '50%'}}>
          {renderTab()}
        </View>
        {/* <TitleHome
          title='My daily task'
          subTitle='Edit'
        /> */}
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 24
  },
  containerTab: {
    flexDirection: 'row'
  }
})

export default Home