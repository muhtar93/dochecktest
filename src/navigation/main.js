import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import WelcomeScreen from '../screens/welcome'
import BottomTab from './bottomtab'
import Goal from '../screens/Goal'
import Profile from '../screens/Profile'

const Stack = createStackNavigator()

const MainTab = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false
      }}
    >
      <Stack.Screen 
        name='Welcome'
        component={WelcomeScreen} 
      />
      <Stack.Screen name='BottomTab' component={BottomTab} />
      <Stack.Screen name="Goal" component={Goal} />
      <Stack.Screen name="Profile" component={Profile} />
    </Stack.Navigator>
  )
}

export default MainTab