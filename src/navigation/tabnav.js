import React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import InProgress from '../screens/InProgress';
import Goal from '../screens/Goal';
import Profile from '../screens/Profile';
import { NavigationContainer } from '@react-navigation/native';

const Tab = createMaterialTopTabNavigator();

function TabNav() {
  return (
    <NavigationContainer independent={true}>
    <Tab.Navigator>
      <Tab.Screen name="Home" component={Goal} />
      <Tab.Screen name="Settings" component={Profile} />
    </Tab.Navigator>
    </NavigationContainer>
  )
}

export default TabNav