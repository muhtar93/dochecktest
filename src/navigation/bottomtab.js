import React from 'react'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import Activity from '../screens/Activity'
import Goal from '../screens/Goal'
import Home from '../screens/Home'
import Profile from '../screens/Profile'
import { Colors } from '../themes'
import {StyleSheet, View, Image, TouchableOpacity} from 'react-native'

const Tab = createBottomTabNavigator()
const CustomTabBarButton = ({children, onPress}) => (
  <TouchableOpacity 
    onPress={onPress}
    style={{
      top: -10,
      justifyContent: 'center',
      alignItems: 'center'
    }}
  >
    <View style={{
      width: 70,
      height: 70,
      borderRadius: 35,
      backgroundColor: 'white'
    }}>
      {children}
    </View>
  </TouchableOpacity>
)

const BottomTab = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        showLabel: false,
        style: {
          position: 'absolute',
          backgroundColor: Colors.white,
          borderRadius: 32,
          ...styles.shadow
        }
      }}
    >
      <Tab.Screen 
        name='Home' 
        component={Home} 
        options={{
          tabBarIcon: ({focused}) => (
            <View style={{alignItems: 'center', justifyContent: 'center', top: 10}}>
              <Image 
                source={require('../images/ic_home.png')}
                resizeMode={'contain'}
                style={{
                  width: 34,
                  height: 34,
                  tintColor: focused? Colors.primary : Colors.primaryText
                }}
              />
            </View>
          )
        }}
      />
      <Tab.Screen 
        name='Goal' 
        component={Goal}
        options={{
        tabBarIcon: ({focused}) => (
            <View style={{alignItems: 'center', justifyContent: 'center', top: 10}}>
              <Image 
                source={require('../images/ic_goal.png')}
                resizeMode={'contain'}
                style={{
                  width: 30,
                  height: 30,
                  tintColor: focused? Colors.primary : Colors.primaryText
                }}
              />
            </View>
          )
        }}
      />
      <Tab.Screen 
        name='Add' 
        component={Goal}
        options={{
          tabBarIcon: ({focused}) => (
            <Image 
                source={require('../images/ic_add.png')}
                resizeMode={'contain'}
                style={{
                  width: 120,
                  height: 120
                }}
              />
          ),
          tabBarButton: (props) => (
            <CustomTabBarButton {...props}/>
          )
        }}
      />
      <Tab.Screen 
        name='Activity' 
        component={Activity}
        options={{
          tabBarIcon: ({focused}) => (
            <View style={{alignItems: 'center', justifyContent: 'center', top: 10}}>
              <Image 
                source={require('../images/ic_calendar.png')}
                resizeMode={'contain'}
                style={{
                  width: 32,
                  height: 32,
                  tintColor: focused? Colors.primary : Colors.primaryText
                }}
              />
            </View>
          )
        }} 
      />
      <Tab.Screen 
        name='Profile' 
        component={Profile}
        options={{
          tabBarIcon: ({focused}) => (
            <View style={{alignItems: 'center', justifyContent: 'center', top: 10}}>
              <Image 
                source={require('../images/ic_profile.png')}
                resizeMode={'contain'}
                style={{
                  width: 32,
                  height: 32,
                  tintColor: focused? Colors.primary : Colors.primaryText
                }}
              />
            </View>
          )
        }}
      />
    </Tab.Navigator>
  )
}

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#7F5DF0',
    shadowOffset: {
      width: 10,
      height: 0
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.5,
    elevation: 2
  }
})

export default BottomTab